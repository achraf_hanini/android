package com.example.readsms;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import androidx.annotation.Nullable;


public class ServiceCommunicator extends Service {
    private SMSreceiver mSMSreceiver;
    private IntentFilter mIntentFilter;
    private static final String SMS_RECEIVED =
            "android.provider.Telephony.SMS_RECEIVED";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        IntentFilter filter = new IntentFilter(SMS_RECEIVED);
        registerReceiver(new SMSreceiver(), filter);


    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        // Unregister the SMS receiver
        if (mSMSreceiver != null) {
            unregisterReceiver(mSMSreceiver);
        }
    }
}
