package com.example.readsms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;

public class SMSreceiver extends BroadcastReceiver {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        ArrayList<ReponseAuto> tabReponse = (ArrayList<ReponseAuto>) intent.getSerializableExtra("test");

        String ac = intent.getAction();
        String strMessage = "";

        if (extras != null) {
            Object[] smsextras = (Object[]) extras.get("pdus");
            if (smsextras != null && smsextras.length > 0) {
                for (int i = 0; i < 1; i++) {
                    SmsMessage smsmsg = SmsMessage.createFromPdu((byte[]) smsextras[i]);

                    String strMsgBody = smsmsg.getMessageBody().toString();
                    String strMsgSrc = smsmsg.getOriginatingAddress();

                    strMessage += "SMS from " + strMsgSrc + " : " + strMsgBody;


                    Log.e("r", "SMS FROM ");
                    SmsManager sender = SmsManager.getDefault();

                if(tabReponse!= null && tabReponse.size() > 0) {

                        for (ReponseAuto r : tabReponse) {
                            ReponseAuto rep = r;
                            if (r.getRecu() == strMsgBody) {
                                String reponse = r.getReponse();
                                sender.sendTextMessage(strMsgSrc, null, r.getReponse(), null, null);
                            } else {
                                sender.sendTextMessage(strMsgSrc, null, "ceci est un répondeur auto à ton message : " + strMsgBody, null, null);
                            }
                        }
                    }else{
                    sender.sendTextMessage(strMsgSrc, null, "ceci est un répondeur auto à ton message : " + strMsgBody, null, null);
                }
                }
            }

        }
    }
}
