package com.example.readsms;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SmsActivity  extends ArrayAdapter<ReponseAuto> {
    private Activity activity;
    private ArrayList<ReponseAuto> lPerson;
    private static LayoutInflater inflater = null;

    public SmsActivity (Activity activity, int textViewResourceId,ArrayList<ReponseAuto> _lPerson) {
        super(activity, textViewResourceId,_lPerson);
        try {
            this.activity = activity;
            this.lPerson = _lPerson;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }

    public int getCount() {
        return lPerson.size();
    }

    public ReponseAuto getItem(ReponseAuto position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {
        public TextView display_name;
        public TextView display_number;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.getsms, null);
                holder = new ViewHolder();

                holder.display_name = (TextView) vi.findViewById(R.id.list_item_string);
                holder.display_number = (TextView) vi.findViewById(R.id.reponse);
                Button delete = vi.findViewById(R.id.delete_btn);

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lPerson.remove(position);
                        notifyDataSetChanged();
                    }
                });


                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }



            holder.display_name.setText(lPerson.get(position).recu);
            holder.display_number.setText(lPerson.get(position).reponse);


        } catch (Exception e) {


        }
        return vi;
    }
}