package com.example.readsms;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

class ReponseAuto implements Serializable {
    String recu;
    String reponse;

    public ReponseAuto(String recu, String reponse) {
        this.recu = recu;
        this.reponse = reponse;
    }

    protected ReponseAuto(Parcel in) {
        recu = in.readString();
        reponse = in.readString();
    }


    public String getRecu() {
        return recu;
    }

    public void setRecu(String recu) {
        this.recu = recu;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }


}
