package com.example.readsms;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.example.myapplication.R;

import java.util.ArrayList;

public class GetSmsList extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_smslist);

ArrayList<ReponseAuto> test = new ArrayList<>();
        ListView v = findViewById(R.id.listdesms);
        test.add(new ReponseAuto("recu","reponse"));
        test.add(new ReponseAuto("recu2","reponse2"));
        SmsActivity adapter = new SmsActivity(GetSmsList.this,0,test);
        v.setAdapter(adapter);
        Intent i = new Intent(this,SMSreceiver.class);
        i.putExtra("test",test);
        String e="test";
        sendBroadcast(i);



    }
}
