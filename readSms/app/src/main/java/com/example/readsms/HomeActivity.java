package com.example.readsms;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.myapplication.R;

public class HomeActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 4200;
    public static final int REQUEST_READ_CONTACTS = 79;

    public static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public static final int RequestPermissionCode = 7;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_home);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {


            Thread loadingThread = new Thread() {

                @Override
                public void run() {
                    try {
                        super.run();
                        sleep(2000);
                    } catch (Exception e) {

                    } finally {

                        Intent main = new Intent(HomeActivity.this, MainActivity.class);
                        startActivity(main);
                        finish();
                    }
                }
            };
            loadingThread.start();
        }
        else{

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS}, RequestPermissionCode);

            Thread loadingThread = new Thread() {

                @Override
                public void run() {
                    try {
                        super.run();
                        sleep(4500);
                    } catch (Exception e) {

                    } finally {

                        Intent main = new Intent(HomeActivity.this, MainActivity.class);
                        startActivity(main);
                        finish();
                    }
                }
            };
            loadingThread.start();

        }
    }
}
