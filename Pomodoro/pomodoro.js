import React from 'react';
import { StyleSheet, Text, View,Button , TextInput,TouchableOpacity,Modal,Animated,Easing} from 'react-native';
import { Ionicons,AntDesign,Entypo } from '@expo/vector-icons';





export class Pomodoro extends React.Component {
  constructor(props) {
    super(props);

    this.state = { secondes : 0o0, minutes :0o0, interval : null , launchDisable : false,
       tempsTravail: 10 , tempsRepos: 10, valider:true, modifier:false, tempsActuel: null,
       modalVisible:false,anim:new Animated.value(0)};
  }

  updateTimer(){
    let secondes = this.state.secondes;
    let minutes = this.state.minutes;
    let tempsActuel = this.state.tempsActuel;
    if(tempsActuel == null ){
      this.setState({
        tempsActuel: "temps de travail",
        minutes: this.state.tempsTravail,
        secondes:0
      })
    }
    if(secondes == 0 && minutes ==0){
      if(tempsActuel=="temps de travail"){

          this.setState({
            modalVisible:true
          });
        setInterval(() => {
          this.setState({
            modalVisible:false
          });
        },1550)
        this.setState({

          minutes: this.state.tempsRepos,
          tempsActuel: "temps de repos",
          secondes:1
        })
      }
      if(tempsActuel=="temps de repos"){
        this.setState({
          modalVisible:true
        });
      setInterval(() => {
        this.setState({
          modalVisible:false
        });
      },1550)
        this.setState({
          minutes: this.state.tempsRepos,
          tempsActuel: "temps de travail",
          secondes:1
        })
      }
      this.setState({
        launchDisable:true,

      })
    }
    else if(secondes == 0 && minutes !=0){
      this.setState({
        minutes : this.state.minutes - 1,
        secondes : 59
      });
    }
    else{
    this.setState({
      secondes : this.state.secondes - 1
    });
  }

  }

  launchTimer(){
    this.setState({
      interval : setInterval( () => this.updateTimer(),1000),
      launchDisable : true
    });
  }

  stopTimer(){
    clearInterval(this.state.interval);
    this.setState({
      launchDisable : false
    })
  }

  resetTimer(){
    let tempsActuel = this.state.tempsActuel;
    let minutes = this.state.minutes;
    let secondes = this.state.secondes;
    this.stopTimer();
    if(tempsActuel == null || tempsActuel == "temps de travail"){
      this.setState({
        tempsActuel: "temps de travail",
        minutes: this.state.tempsTravail,
        secondes:0
      });

      }
      else{
        this.setState({
          tempsActuel: "temps de repos",
          minutes: this.state.tempsRepos,
          secondes:0
        })
    }
  }

  modification(){
    this.setState({
      modifier:true,
      valider:false
    });
  }

  validation(){
    let minutes = this.state.minutes;
    let tempsActuel= this.state.tempsActuel;
    this.setState({
      modifier:false,
      valider:true
    });
    this.resetTimer();
    this.launchTimer();
  }

  render(){
    return(

      <View>
        <Text style={( (this.state.secondes < 21 && this.state.minutes == 0 ) ) ? {fontSize:30,textAlign:"center",fontWeight:"bold",textDecorationLine:"underline",color:"red"} : {fontSize:30,textAlign:"center",fontWeight:"bold",textDecorationLine:"underline"}}>
        {this.state.minutes} : {this.state.secondes}
        </Text>
        <TouchableOpacity onPress={() => this.launchTimer()} disabled={this.state.launchDisable} style={{backgroundColor:"green",height:30}} ><Text style={{color:"white",fontSize:20,textAlign:"center"}}>Start<AntDesign name="caretright" size={17} color="black" /></Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.stopTimer()} style={{backgroundColor:"red",height:30}} ><Text style={{color:"white",fontSize:20,textAlign:"center"}}>Stop <Entypo name="controller-paus" size={17} color="black" /></Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.resetTimer()}  style={{backgroundColor:"orange",height:30}}><Text style={{color:"white",fontSize:20,textAlign:"center"}}>Reset <AntDesign name="reload1" size={17} color="black" /></Text></TouchableOpacity>
        <Text>Temps de travail : </Text>
        <TextInput keyboardType={'numeric'} editable={this.state.modifier} style={{height: 20, borderColor: 'blue', borderWidth: 1 ,margin:5 }}
         onChangeText={tempsTravail => this.setState({tempsTravail})} value={`${this.state.tempsTravail}`} />
         <Text>Temps de repos : </Text>
         <TextInput keyboardType={'numeric'} editable={this.state.modifier} style={{height: 20, borderColor: 'blue', borderWidth: 1 ,margin:5 }}
          onChangeText={tempsRepos => this.setState({tempsRepos})} value={`${this.state.tempsRepos}`} />
          <Button title="Edit" disabled={this.state.modifier} onPress={() => this.modification()}/>
          <TouchableOpacity  disabled={this.state.valider} onPress={() => this.validation()} style={{backgroundColor:'#4BCCE5', height:30}} ><Text style={{color:"white",fontSize:20,textAlign:"center"}}>Valider</Text></TouchableOpacity>
          <Text >
          Temps de repos : {this.state.tempsRepos} {"\n"}
          temps de travail : {this.state.tempsTravail}
          </Text>
          <Text style={{ color:'red', fontSize: 30,fontWeight:"bold"}}>
          {"\n"}
          {this.state.tempsActuel}
          </Text>

          <Modal
  animationType={"slide"}
  transparent={false}
  visible={this.state.modalVisible}>
  <Text style={{ color:'red', fontSize: 30,fontWeight:"bold",textAlign:"center"}}>changement de temps ➟ {this.state.tempsActuel}</Text></Modal>




      </View>

    );
  }

}
