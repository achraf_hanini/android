package com.example.tabbedapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
    List<Task> lesTaches;

    public MyRecyclerViewAdapter(List<Task> lesTaches) {
        this.lesTaches = lesTaches;
    }

    public void add(Task t){
        lesTaches.add(t);
        notifyDataSetChanged();
    }

    public void update(List<Task> items) {
        lesTaches.clear();
        lesTaches.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Display(lesTaches.get(position));
    }

    @Override
    public int getItemCount() {
        return lesTaches.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
    private TextView desc;
    private TextView titre;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        desc = itemView.findViewById(R.id.textView);
        titre = itemView.findViewById(R.id.ItemText);
    }

    public void Display (Task tache){
        desc.setText(tache.getDesc());
        titre.setText(tache.getTitre());

    }
}

}