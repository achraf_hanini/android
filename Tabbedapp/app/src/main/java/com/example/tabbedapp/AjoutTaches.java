package com.example.tabbedapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.tabbedapp.ui.main.SectionsPagerAdapter;

import org.w3c.dom.Text;

import java.lang.reflect.Array;

public class AjoutTaches extends Fragment {
    Button addB;
    EditText titreEdit;
    EditText descEdit;
    Bundle bundle;
    String titre;
    String desc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.form_taches,container,false);
        addB = v.findViewById(R.id.addButton);

        titreEdit = v.findViewById(R.id.titre);
        descEdit = v.findViewById(R.id.desc);

        addB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).passData("test");
                titre = titreEdit.getText().toString();
                desc = descEdit.getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("titre",titre);
                bundle.putString("desc",desc);
                ListeTaches li = new ListeTaches();

                li.setArguments(bundle);
                Log.e("t","test : " + bundle);
//                getFragmentManager().beginTransaction().replace(R.id.view_pager,li);
                //getFragmentManager().beginTransaction().commit();
                //li.addTask(new Task(titre,desc));
                //((MainActivity)getActivity()).selectTab(2);
                //((MainActivity)getActivity()).envoiFunction(new Task(titre,desc));

            }
        });

        return v;
    }
}
