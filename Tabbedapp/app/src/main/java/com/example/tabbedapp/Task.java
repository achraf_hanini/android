package com.example.tabbedapp;

public class Task {
    private String Titre;
    private String Desc;

    public Task(String titre, String desc) {
        Titre = titre;
        Desc = desc;
    }

    public String getTitre() {
        return Titre;
    }

    public void setTitre(String titre) {
        Titre = titre;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }
}
