package com.example.tabbedapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ListeTaches extends Fragment  {
    private RecyclerView listI;
    private List<Task> items = new ArrayList<Task>();
    private Task tacheClass;
    private MyRecyclerViewAdapter  adapter = new MyRecyclerViewAdapter(items);
    private String titre;
    private String desc;
    private TextView textView2;
    private View v;
    Bundle args ;


    public void onFragmentInteraction(String uri) {
        Log.d("sai",uri);
        String t = "kool";

    }

    public void testfragment(){
        Bundle args = getArguments();
        String text = "tet";

        //  items.add(tache);
        if (getArguments() != null) {
            titre = getArguments().getString("titre");
            desc = getArguments().getString("desc");
            Task t = new Task("titreee", "desc");
            t.setTitre(titre);
            t.setDesc(desc);
            items.add(t);
            Log.e("t", "Oui je peux :" + t.getTitre() + "list " + items);
            adapter.add(t);
            adapter.notifyDataSetChanged();
        }

    }

    public void displayText(String text){
        ((TextView)getActivity().findViewById(R.id.textView2)).setText(text);
        Log.e("t","displayText : " + text);


    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.taches, container, false);


        listI = v.findViewById(R.id.monRecyl);
    listI.setHasFixedSize(true);

Log.e("t","final :" + getArguments());
testfragment();
afficherListe();



       return v;
    }



    public void afficherListe(){
        listI.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        listI.setAdapter(adapter);


    }






    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void addTask(Task task) {
        items.add(task);
    }
}
