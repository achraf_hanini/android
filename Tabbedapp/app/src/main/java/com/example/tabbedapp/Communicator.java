package com.example.tabbedapp;

interface Communicator {

    public void passData (String text);

}
