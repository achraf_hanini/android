import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { StyleSheet, Text, View,TextInput,TouchableOpacity } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';

export default class LinksScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      nom:'',
      description:'',
    };
  }

  render(){
  return (
    <View >
  <TextInput placeholder=" enter Name" style={styles.formInput}/>
  <TextInput placeholder=" enter description" style={styles.formInput}/>
  <TouchableOpacity style={{width:"80%",
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    margin:40}}><Text style={{textAlign:'center',borderColor:'black',margin:5,height:25,fontSize:18,fontWeight:'bold'}}>Valider</Text></TouchableOpacity>
    </View>
  );
 }
}



const styles = StyleSheet.create({
  formInput:{
    borderWidth:1,
    borderColor:'black',
    margin:15,
    borderRadius:3,
    backgroundColor:'white',
    height:35,
    textAlign:'center'
  }
});
