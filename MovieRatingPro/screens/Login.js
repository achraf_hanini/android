import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TextInput,TouchableOpacity, View,FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { MonoText } from '../components/StyledText';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state={login:null,password:'',connected:false};

  }

  connexion(){
    if(this.state.login=="login" && this.state.password=="password"){
      this.setState({
        connected:true
      })
    }
  }

  isConnected(){
    return this.state.connected
  }

  render(){
  return (
    <View style={{alignItems:'center'}}>
        <Text>Page de connexion</Text>
        <TextInput style={styles.formInput} placeholder="enter login" onChangeText={login => this.setState({login})}/>
        <TextInput style={styles.formInput} placeholder="enter password" onChangeText={password => this.setState({password})}/>

        <TouchableOpacity style={{margin:50,alignItems:'center',borderColor:'black',
        borderWidth:1,height:25,justifyContent:"center",width:50,borderRadius:3,backgroundColor:'green'}} onPress={() => this.connexion()}><Text>Valider</Text></TouchableOpacity>
        <Text>Login : {this.state.login}, password : {this.state.password}</Text>
        </View>



      );
    }
}

const styles = StyleSheet.create({
  formInput:{
    borderWidth:1,
    borderColor:'black',
    margin:15,
    borderRadius:3,
    backgroundColor:'white',
    height:35,
    textAlign:'center'
  }
});
