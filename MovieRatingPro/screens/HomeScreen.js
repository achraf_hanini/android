import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View,FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabs:[]};
  }
  componentDidMount =()=>{
         fetch("https://api.themoviedb.org/3/movie/top_rated?api_key=2387daccd95122cbb2fb9085e8c950ff")
             .then((response) => response.json())
             .then((responseJson) => {
                 console.log(responseJson.results);
                 this.setState({tabs:responseJson.results});
             })
             .catch((error) => {
                 console.error(error);
             });

   }
  render(){
  return (
    <View >

      <FlatList
    style={{width: '100%', height:'100%'}}
    horizontal={false}
      data={this.state.tabs}
      renderItem={({item}) =>
      <View style={{backgroundColor:'#f9c2f0',borderColor:'black',borderBottomWidth:1,borderTopWidth:1}}>
      <Image source={{uri: 'http://image.tmdb.org/t/p/original'+item.poster_path}}
       style={{width: 80, height: 120}} />
       <Text>TITRE :                                      Note : {item.vote_average}</Text>
        <Text style={styles.item}> {item.title}</Text><Text>──────────────── </Text>
        <Text>{item.overview}</Text>
        </View>
      }

    />

    </View>
      );
    }
}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({

  item: {
    backgroundColor: 'white',
    padding: 0,
    marginVertical: 8,
    marginHorizontal: 16,
    width:120,
    height:50,
    textAlign:'center'
  },
  title: {
    fontSize: 32,
  },
});
